from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

options = webdriver.ChromeOptions()
options.add_experimental_option("detach", True)

browser = webdriver.Chrome(options=options)
browser.get('https://coin98.net/nguoi-moi/detail?subCategory=648a79e8c54e0dea470f2e71')

wait = WebDriverWait(browser, 10)
a_elements = wait.until(EC.presence_of_all_elements_located((By.TAG_NAME, 'a')))
num_of_a_elements = len(a_elements)

print("Số lượng thẻ <a> trên trang là:", num_of_a_elements)
